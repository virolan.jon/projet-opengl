# OpenGL Project - 

# Mazeran Paul & Jonathan Virolan

The project is a model an urban environment using OpenGL. We have create a random word composed by one random forest, 2 different residential districts, 1 tower neighborhood and one car.

## Implemented modules
+ Dynamic light : 50%
    - Sun moving in the sky.   
+ Realistic camera : 100%
    - Constraint movement on the ground
    - Visualisation from a motionless point in the air 
    - Totally free move (3D movements)

(The view change is done by the space key)
+ Random word generation : 100%
    - Random forest
    - Two random residential districs
+ Planned moves : 100%
    - A car with a random route (randomly chosen from 3 routes)

## HOW TO USE IT?

### Compilation
Open a terminal locally.
```shell
$ git clone https://gitlab.com/virolan.jon/projet-opengl.git
$ cd projet-opengl
$ cmake ./
$ make
& ./ProjetOpenGL_Mazeran_Virolan
```
### Utilisation
Use the arrows to move around.

Press the space key to change views

Press the escape key to exit. 