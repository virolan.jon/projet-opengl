#include <iostream>
#include <string>
#include <stdlib.h>
#include <ctime>
#include <vector>


// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Shader loading class
#include "Shader.h"
// Camera loading class
#include "Camera.h"
// Model loading class
#include "Model.h"

// Other Libs
#include <SOIL.h>


// Dimension of the window
const GLuint WIDTH = 800, HEIGHT = 600;

//Time of one cycle (Change if we want that day/night is longer or shorter)
GLint TIME_CYCLE(120);

// Angle to know if are in twilight or dawn
GLfloat ANGLE_TWILIGHT(3*M_PI/5);
GLfloat ANGLE_DAWN(4*M_PI/3);

//Number of zone
//GLint nbZone = 1;

int main()
{
    glfwInit();

    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Necesary for Mac OS
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // Create the application window
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Projet OpenGL Mazeran & Virolan", nullptr, nullptr);
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }

    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);

    // Variable global that allows to ask the GLEW library to find the modern
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }

    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);

    // Allow to test the depth information
    glEnable(GL_DEPTH_TEST);
    glCullFace(GL_BACK);

    // Build and compile our vertex and fragment shaders
    Shader shaders("shaders/c3/default.vertexshader",
        "shaders/c3/default.fragmentshader");

    GLint nbTriangles;

    //Ground definitions,  sqare from -500, -500 to 500, 500
    GLfloat vertices[] = {
        /*     Positions    |      Normales     |     UV     */
        -100.0f,  0.0f, -100.0f,   0.0f, 1.0f, 0.0f,   0.0f, 1250.0f, // Top Left
        -100.0f,  0.0f,  100.0f,   0.0f, 1.0f, 0.0f,   0.0f, 0.0f, // Bottom Left
         100.0f,  0.0f, -100.0f,   0.0f, 1.0f, 0.0f,   1250.0f, 1250.0f, // Top Right
         100.0f,  0.0f,  100.0f,   0.0f, 1.0f, 0.0f,   1250.0f, 0.0f,  // Bottom Right
    };

    GLshort indices[]{
        0, 1, 2,
        1, 3, 2,
    };

    nbTriangles = sizeof(indices)/3;


    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO, VBO, EBO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(1, &EBO);

    // Bind the VAO
    glBindVertexArray(VAO);
    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
        (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // UV coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
        (GLvoid*)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Declare the texture identifier
    GLuint texture;
    // Generate the texture
    glGenTextures(1, &texture);
    // Bind the texture created in the global context of OpenGL
    glBindTexture(GL_TEXTURE_2D, texture);
    // Method of wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Filtering method
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Loading the image file using the SOIL lib
    int twidth, theight;
    unsigned char* data = SOIL_load_image("texture/grassOGL.jpg",
        &twidth, &theight, 0, SOIL_LOAD_RGB);
    // Associate the image data with texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight,
        0, GL_RGB, GL_UNSIGNED_BYTE, data);
    // Generate the mipmap
    glGenerateMipmap(GL_TEXTURE_2D);
    // Free the memory
    SOIL_free_image_data(data);
    // Unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);

    // Camera
    Camera camera(glm::vec3(0.0f, 1.0f, 0.0f), window);

    //import obj
    Model house("model/house/house.obj");
    Model tower("model/tower/7z_OBJ_Panel_House_Wikipedia_of_Russia_01/Panel_House_Wikipedia_of_Russia_01.obj");
    Model sun("model/Sun/soleil.obj");
    Model streetlamp("model/streetlamp/Lamp.obj");
    Model lightedstreetlamp("model/streetlamp/lightedstreetlamp/Lamp.obj");
    Model Tree1("model/Tree1/Tree1.3ds");
    Model road("model/road/route3.obj");
    Model car("model/Car/car.obj");

    srand(std::time(NULL));
    int nbTreeHouse1 = rand()%5 + 5;
    int nbTreeHouse2 = rand()%5 + 5;
    std::vector<std::string> typeZone = {"House1","House2","Tree1","Tower"};
    std::random_shuffle(typeZone.begin(), typeZone.end());
    GLfloat xZone[4];
    GLfloat zZone[4];

    // Bottom Left side of the Square 1
    xZone[0] = -75.0f;
    zZone[0] = -25.0f;
    // Bottom Left side of the Square 2
    xZone[1] = 25.0f;
    zZone[1] = -25.0f;
    // Bottom Left side of the Square 3
    xZone[2] = 25.0f;
    zZone[2] = 75.0f;
    // Bottom Left side of the Square 4
    xZone[3] = -75.0f;
    zZone[3] = 75.0f;

    //create table probabilities for random forest
    srand(std::time(NULL));
    std::vector<int> tab_proba;
    for(int i =0; i < 32; i++)
    {
        int p = rand()%5;
        tab_proba.push_back(p);

    }

    //car movment initialisation
    glm::vec3 posCar = glm::vec3(-100.0f,0.59f,0.0f);
    GLfloat vitesse = 0.2f;
    int car_itinerary = rand()%3;



    // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {
        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();
        camera.Do_Movement();
        camera.Switch_Mode();

        glUniform3f(glGetUniformLocation(shaders.Program, "viewPos"), camera.Position.x, camera.Position.y, camera.Position.z);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Inform OpenGL that we want to use the shaders we have created
        shaders.Use();

        // Recover the identifiers of the global variables of the shader
        GLint modelLoc = glGetUniformLocation(shaders.Program, "model");

        // Camera matrix
        glm::mat4 view;
        view = camera.GetViewMatrix();
        // Projection matrix (generate a perspective projection matrix)
        glm::mat4 projection;
        projection = glm::perspective(45.0f, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 350.0f);

        // Model matrix (translation, rotation and scale)
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        // Activate texture 0
        glActiveTexture(GL_TEXTURE0);
        // Bind the texture
        glBindTexture(GL_TEXTURE_2D, texture);
        // Associate the texture with the shader
        glUniform1i(glGetUniformLocation(shaders.Program, "modelTexture"), 0);
        // Bind the VAO as a current object in the context of OpenGL
        glBindVertexArray(VAO);

        // -------- Sun ----------
        // define the position and the ligth of the sun
        GLint lightPos = glGetUniformLocation(shaders.Program, "lightPos");
        GLint lightColor = glGetUniformLocation(shaders.Program, "lightColor");
        GLint ambientStrength = glGetUniformLocation(shaders.Program, "ambientStrength");
        GLint specularStrength = glGetUniformLocation(shaders.Program, "specularStrength");
        GLfloat ambStr(0.05f);
        GLfloat specStr(0.3f);

        //Positionning the sun in function of the camera to follow the user
        glm::vec4 lPos(camera.Position.x, camera.Position.y + 150.0f, camera.Position.z, 1.0f);
        glm::vec3 lColor(1.0f, 1.0f, 1.0f);

        //Rotation Matrix of the sun, allow the sun to rotate arround Z-axis
        glm::mat4 rot = glm::mat4(1.0f);
        GLfloat angle = glm::mod(2*M_PI*(glfwGetTime()/TIME_CYCLE), 2*M_PI);
        rot = glm::rotate(rot, angle, glm::vec3(0.0f, 0.0f, 1.0f));
        lPos = rot * lPos;

        glm::vec3 clearColor;
        // Here, we modify the sky color, the ambient light if we are at night or in the day (and between both).
        if (angle > ANGLE_TWILIGHT && angle < ANGLE_DAWN){
            lColor = glm::vec3(0.0f, 0.0f, 0.05f);

            clearColor = glm::vec3(0.0f, 0.0f, 0.1f);

            ambStr = 0.05f;

        } else if (angle > M_PI/4 && angle < ANGLE_TWILIGHT){

            double x = (ANGLE_TWILIGHT - angle)/(ANGLE_TWILIGHT - M_PI/4);

            lColor = glm::vec3(x * 1.0f, pow(x, 2) * 1.0f, pow(x, 4) * 1.0f + 0.05f);

            clearColor = glm::vec3((-2*pow(x,2) + 2*x) * 1.0f, pow(x, 2) * 0.5f, pow(x, 4) * 1.0f + 0.1f);

            ambStr = x * 0.65f + 0.05f;


        } else if (angle > ANGLE_DAWN && angle < 5*M_PI/3){
            double x = (ANGLE_DAWN - angle)/(ANGLE_DAWN - 5*M_PI/3);

            lColor = glm::vec3(pow(x,4) * 1.0f, pow(x, 2) * 1.0f, x * 1.0f + 0.05f);

            clearColor = glm::vec3((-2*pow(x,2) + 2*x) * 1.0f, pow(x, 2) * 0.5f, pow(x, 4) * 1.0f + 0.1f);

            ambStr = x * 0.65f + 0.05f;


        } else{
            lColor = glm::vec3(1.0f, 1.0f, 1.0f);

            clearColor = glm::vec3(0.0f, 0.5f, 1.0f);

            ambStr = 0.7f;
        }

        // The color sky is changed.
        glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);
        glUniform3f(lightPos, lPos.x, lPos.y, lPos.z);
        glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);
        glUniform1f(ambientStrength, ambStr);
        glUniform1f(specularStrength, specStr);

        glUniformMatrix4fv(glGetUniformLocation(shaders.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(glGetUniformLocation(shaders.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        // Light of the streetlamp.
        GLint lampPos = glGetUniformLocation(shaders.Program, "lampPos");
        GLint lampColor = glGetUniformLocation(shaders.Program, "lampColor");

        glm::vec3 lmpColor;
        // Take care of the streetlamp color.
        if (angle > 2*M_PI/5 && angle < 8*M_PI/5){
            lmpColor = glm::vec3(1.0f, 0.56f, 0.17f);
        } else {
            lmpColor = glm::vec3(0.0f, 0.0f, 0.0f);
        }
        glUniform3f(lampColor, lmpColor.x, lmpColor.y , lmpColor.z);

        // Position of the streetlamp.
        glm::vec3 lmpPos(0.0f, 1.6f, -4.0f);
        glUniform3f(lampPos, lmpPos.x, lmpPos.y , lmpPos.z);



    // Drawing =====================

        // --------- Draw the current object (GROUND)
        glDrawElements(GL_TRIANGLES, 3*nbTriangles, GL_UNSIGNED_SHORT, 0);

        // --------- Draw the sun
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(camera.Position.x, camera.Position.y + 150.0f, camera.Position.z));
        model = rot*model;
        model = glm::scale(model, glm::vec3(13.0f));
        glUniform1f(ambientStrength, 5.0f);
        glUniform3f(lampColor, 0.0f, 0.0f , 0.0f);
        glUniform3f(lightColor, lColor.x + 0.4f, clearColor.y, 0.0f);
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        sun.Draw(shaders);
        glUniform3f(lampColor, lmpColor.x, lmpColor.y , lmpColor.z);
        glUniform1f(ambientStrength, ambStr);
        glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);



        // --------- Draw the streetlamp or the lighted streetlamp.
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(lmpPos.x, 0.0f, lmpPos.z));
        model = glm::scale(model, glm::vec3(1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        if(lmpColor.x != 0.0f){ // If it's the night, the street lamp is lighted.
            glUniform1f(ambientStrength, 0.7f);
            glUniform3f(lightColor, lmpColor.x, lmpColor.y, lmpColor.z);

            lightedstreetlamp.Draw(shaders);
            glUniform1f(ambientStrength, ambStr);
            glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);


        } else{ // If not, the streetlamp is not lighted.
            streetlamp.Draw(shaders);
        }

        glUniform3f(lampColor, lmpColor.x, lmpColor.y , lmpColor.z);

        for (int i = 0 ; i < 4 ; i++){
            if (typeZone[i] == "House1"){
                //Draw some house1
                GLfloat x, y, z;
                for (int nbhouse(0); nbhouse <16; nbhouse++){
                    if (nbhouse < 8){
                        model = glm::mat4(1.0f);
                        x = xZone[i] - 10.0f + nbhouse*10.0f;
                        z = zZone[i] - 15.0f;
                        model = glm::translate(model, glm::vec3(x, 0.0f, z));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(0.1f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        house.Draw(shaders);
                        model = glm::mat4(1.0f);
                        model = glm::translate(model, glm::vec3(x - 3.0f, 0.0f, z - 10.0f));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        Tree1.Draw(shaders);
                    }
                    else{
                        model = glm::mat4(1.0f);
                        x = xZone[i] - 15.0f + (nbhouse-8)*10.0f;
                        z = zZone[i] + 10.0f;
                        model = glm::translate(model, glm::vec3(x, 0.0f, z));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(0.1f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        house.Draw(shaders);
                    }
                }
            }
            else if (typeZone[i] == "House2"){
                GLfloat x, y, z;
                for (int nbhouse(0); nbhouse <6; nbhouse++){
                    model = glm::mat4(1.0f);
                    x = xZone[i] - 10.0f + (nbhouse%3)*10.0f;
                    z = zZone[i] - 15.0f + floor(nbhouse/2)*12.5f;
                    model = glm::translate(model, glm::vec3(x, 0.0f, z));
                    model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                    model = glm::scale(model, glm::vec3(0.1f));
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    house.Draw(shaders);
                }
            }
            else if(typeZone[i] == "Tower"){
                model = glm::mat4(1.0f);
                model = glm::translate(model, glm::vec3(xZone[i], 0.0f, zZone[i]));
                model = glm::scale(model, glm::vec3(0.3f));
                glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                tower.Draw(shaders);

                model = glm::mat4(1.0f);
                model = glm::translate(model, glm::vec3(xZone[i] + 15.0f, 0.0f, zZone[i] - 35.0f));
                model = glm::scale(model, glm::vec3(0.3f));
                glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                tower.Draw(shaders);

                model = glm::mat4(1.0f);
                model = glm::translate(model, glm::vec3(xZone[i] + 55.0f, 0.0f, zZone[i] - 15.0f));
                model = glm::scale(model, glm::vec3(0.3f));
                glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                tower.Draw(shaders);
            }
            else{
                for (int tree(0); tree < 32; tree++){


                    if(tree < 8 && tab_proba[tree]>=1)
                    {
                        model = glm::mat4(1.0f);
                        model = glm::translate(model,
                                               glm::vec3(xZone[i] + 8*tree,
                                                         0.0f,
                                                         zZone[i]));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(tab_proba[tree]*1.5+.0f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

                        // Draw
                        Tree1.Draw(shaders);
                    }
                    if(tree >= 8 && tree < 16 && tab_proba[tree]>=1)
                    {
                        model = glm::mat4(1.0f);
                        model = glm::translate(model,
                                               glm::vec3(xZone[i] + 8*(tree-8),
                                                         0.0f,
                                                         zZone[i] -15.0f));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(tab_proba[tree]*1.5+.0f));

                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

                        //Draw
                        Tree1.Draw(shaders);
                    }
                    if(tree >= 16 && tree < 24 && tab_proba[tree]>=1)
                    {
                        model = glm::mat4(1.0f);
                        model = glm::translate(model,
                                               glm::vec3(xZone[i] + 8*(tree-16),
                                                         0.0f,
                                                         zZone[i] -30.0f));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(tab_proba[tree]*1.5+.0f));
                        // On remet a jour la variable globale du shader
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

                        //Draw
                        Tree1.Draw(shaders);
                    }
                    if(tree >= 24 && tree < 32 && tab_proba[tree]>=1)
                    {
                        model = glm::mat4(1.0f);
                        model = glm::translate(model,
                                               glm::vec3(xZone[i] + 8*(tree-24),
                                                         0.0f,
                                                         zZone[i] -45.0f));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(tab_proba[tree]*1.5+.0f));

                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

                        //Draw
                        Tree1.Draw(shaders);
                    }

                }
            }
        }
        //croos Road
        for(int i = -5 ; i<6;i++)
        {

            model = glm::mat4(1.0f);
            model = glm::translate(model, glm::vec3(0.0f , 0.01f, i*15.0f));
            model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(0.0f, -1.0f, 0.0f));
            model = glm::scale(model, glm::vec3(2.0f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            road.Draw(shaders);
            model = glm::mat4(1.0f);
            model = glm::translate(model, glm::vec3(i*14.0f , 0.01f,0.0f));
            model = glm::scale(model, glm::vec3(2.0f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            road.Draw(shaders);

        }


        //car mouvment
        model = glm::mat4(1.0f);
        if(posCar.x <= 0.0f) {
                    posCar.x += vitesse;
                    model = glm::translate(model, posCar);
                    model = glm::rotate(model, (GLfloat)M_PI/2,glm::vec3(0.0f,1.0f,0.0f));
                }
        else if(posCar.x <= 100.0f && car_itinerary == 0){
            posCar.x += vitesse;
            model = glm::translate(model, posCar);
            model = glm::rotate(model, (GLfloat)M_PI/2,glm::vec3(0.0f,1.0f,0.0f));

                }
        else if(posCar.z <= 50.0f && car_itinerary == 1 ){
            posCar.z += vitesse;
            model = glm::translate(model, posCar);

                }
        else if(posCar.z >= -50.0f && car_itinerary == 2){
            posCar.z -= vitesse;
            model = glm::translate(model, posCar);
            model = glm::rotate(model, (GLfloat)M_PI,glm::vec3(0.0f,1.0f,0.0f));
                }
        else{
            posCar.x = -100.0f;
            posCar.y = 0.59f;
            posCar.z = 0.0f;
            model = glm::rotate(model, (GLfloat)M_PI/2,glm::vec3(0.0f,1.0f,0.0f));
        }

            model = glm::scale(model, glm::vec3(0.007f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
            glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
            car.Draw(shaders);


        // VBA is detached from the current object in the OpenGL context
        glBindVertexArray(0);

        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);

        //
    }


    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();
    return 0;
}
