# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/adler32.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/adler32.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/compress.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/compress.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/crc32.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/crc32.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/deflate.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/deflate.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/gzclose.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/gzclose.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/gzlib.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/gzlib.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/gzread.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/gzread.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/gzwrite.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/gzwrite.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/infback.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/infback.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/inffast.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/inffast.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/inflate.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/inflate.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/inftrees.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/inftrees.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/trees.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/trees.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/uncompr.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/uncompr.o"
  "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/zutil.c" "/Users/evelynpaiz/Documents/PhD/Code/opengl-course/external/assimp-5.0.0/contrib/zlib/CMakeFiles/zlibstatic.dir/zutil.o"
  )
set(CMAKE_C_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GLEW_STATIC"
  "TW_NO_DIRECT3D"
  "TW_NO_LIB_PRAGMA"
  "TW_STATIC"
  "_CRT_SECURE_NO_WARNINGS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "external/glfw-3.3/include/GLFW"
  "external/glew-2.1.0/include"
  "external/assimp-5.0.0/contrib/zlib"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
